# Mullvad Linux Installer

It's what it sounds like. Run this bash script to install Mullvad on a Linux system. Be sure to run `chmod 700 update-mullvad` to make the file executable. Similarly, make it available to your path via your `~/.bash_profile` or `~/.bash_aliases` files.

Use is simple:

```bash
update-mullvad
```

To install the beta version, use the `beta` subcommand.

```bash
update-mullvad beta
```

It is recommended to disconnect Mullvad before updating, as well as any applications sending sensitive requests.
